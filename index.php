<?php
namespace eMAGHero;

use eMAGHero\PlayGame;
use eMAGHero\viewResults;

include_once 'autoload.php';

class startGame {

	function __construct() {

		$heroProperties = new heroProperties();
		$game = new PlayGame();
		$game->init();

		$viewResulg = new viewResults();
		$viewResulg->view();
		
	}

}

new startGame();

