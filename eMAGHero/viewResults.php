<?php
namespace eMAGHero;

use eMAGHero\heroProperties;

class viewResults extends PlayGame {
  
  public function view () {
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>eMAGHero</title>
      
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
      
      <style>
        .progress {
          margin-top: 7px;
        }
      </style>
    
    </head>
    <body>
    
    
      <div class="row">
        <div class="col-sm-6">
          <h3>Orderus</h3>
        </div>
        <div class="col-sm-6">
          <h3>Wild beasts</h3>
        </div>
      </div>
    
      <?php
      
      $winner = true;
      
      if (self::$result) {
        foreach (self::$result as $key => $round) {
          ?>
        
          <div class="row">
            <div class="col-sm-12">
              <h4><b>Round <?php echo $key; ?></b></h4>
            </div>
          </div>
        
          <div class="row">
          <?php
          if ($round) {
            foreach ($round as $prop => $item) {
              ?>
              <div class="col-sm-6">
              
                <?php
                if ($item) {
                  $count = 0;
                  foreach ($item as $name => $values) {
	                  $count++;
                    if ( $values > 0 ) {
                      ?>
                      <div class="progress">
                        <div class="progress-bar"
                             style="width:<?php echo $values; ?>%"><?php echo $name; ?> <?php echo $values; ?>%
                        </div>
                      </div>
	                    <?php
                    } else {
	                    $winner = false;
                      echo "this player lost";
                    }
                  }
                  ?>
                  <?php
                }
                ?>
            
              </div>
              <?php
            }
            ?>
          
            </div>
            <?php
          }
        }
      }
      
      if ( $winner ) {
        $item = count(self::$result) - 1;
	
	      if ( self::$result[$item]['orderus']['Health'] > self::$result[$item]['wildBeasts']['Health'] ) {
	        echo "Orderus WINNER!!!";
        } elseif ( self::$result[$item]['orderus']['Health'] < self::$result[$item]['wildBeasts']['Health'] ) {
		      echo "Wild Beasts WINNER!!!";
        } else {
	        echo "Equality";
        }
      }
      
      ?>
      
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    
    </body>
    </html>
  <?php
  }
}