<?php
namespace eMAGHero;

use eMAGHero\heroProperties;

class PlayGame extends heroProperties {

	public static $firstPlayer = 'orderus';
	public static $lastPlayer = 'wildBeast';
	
	public static $result;
	
	function init() {
		$this->initGame();
		$this->startGame();
	}
	
	protected function initGame() {
		
		if ( self::$orderusSpeed < self::$wildBeastSpeed ) {
			self::$firstPlayer = 'wildBeast';
			self::$lastPlayer = 'orderus';
		} elseif ( self::$orderusSpeed == self::$wildBeastSpeed ) {
			if ( self::$orderusLuck < self::$wildBeastLuck ) {
				self::$firstPlayer = 'wildBeast';
				self::$lastPlayer = 'orderus';
			}
		}
		
		self::$result = array(
			array(
				'orderus' => array(
					'Health' => self::$orderusHealth,
					'Strength' => self::$orderusStrength,
					'Defence' => self::$orderusDefence,
					'Speed' => self::$orderusSpeed,
					'Luck' => self::$orderusLuck
				),
				'wildBeast' => array(
					'Health' => self::$wildBeastHealth,
					'Strength' => self::$wildBeastStrength,
					'Defence' => self::$wildBeastDefence,
					'Speed' => self::$wildBeastSpeed,
					'Luck' => self::$wildBeastLuck
				)
			)
		);
		
	}

	protected function startGame () {
		
		$player1Health = self::$firstPlayer . 'Health';
		$player2Health = self::$lastPlayer . 'Health';
		
		$count = 0;
		
		while ( self::$$player1Health > 0 and self::$$player2Health > 0 ) {
			
			$count++;
			
			$player1Strength = self::$firstPlayer . "Strength";
			$player2Strength = self::$lastPlayer . "Strength";
			
			$paguba = $this->calculateDamage (self::$firstPlayer,
				self::$$player1Strength);
			
			self::$$player2Health = self::$$player2Health - $paguba;
			
			if ( self::$$player2Health > 0 ) {
				
				$paguba1 = $this->calculateDamage (self::$lastPlayer,
					self::$$player2Strength);
				
				self::$$player1Health = self::$$player1Health - $paguba1;
			}
			
			self::$result[] = array(
				'orderus' => array(
					'Health' => self::$orderusHealth
				),
				'wildBeast' => array(
					'Health' => self::$wildBeastHealth
				)
			);

			if ( $count == self::MAGIC_SHIELD ) break;

		}
		
	}
	
	protected function calculateDamage ( $user, $strength ) {
		
		$luck = $user."Luck";
		
		if ( $this->calculateRand( self::$$luck ) ) {
			return 0;
		}
		
		if ( $user == 'orderus' ) {
		
			if ( $this->calculateRand( self::RAPID_STRIKE ) ) {
				$strength *= 2;
			}
		
		} else {
			
			if ( $this->calculateRand( self::MAGIC_SHIELD ) ) {
				$strength /= 2;
			}
		}

		return $strength;
		
	}
	
}