<?php

namespace eMAGHero;

class heroProperties {
	
	const MAX_ROUNDS = 20;
	
	const MIN = 0;
	const MAX = 100;
	
	const RAPID_STRIKE = 10;
	const MAGIC_SHIELD = 20;
	
	public static $orderus = array(
		'Health'   => array(
			'min' => 70,
			'max' => 100
		),
		'Strength' => array(
			'min' => 70,
			'max' => 80
		),
		'Defence'  => array(
			'min' => 45,
			'max' => 55
		),
		'Speed'    => array(
			'min' => 40,
			'max' => 50
		),
		'Luck'     => array(
			'min' => 10,
			'max' => 30
		)
	);
	
	public static $wildBeast = array(
		'Health'   => array(
			'min' => 60,
			'max' => 90
		),
		'Strength' => array(
			'min' => 60,
			'max' => 90
		),
		'Defence'  => array(
			'min' => 40,
			'max' => 60
		),
		'Speed'    => array(
			'min' => 40,
			'max' => 60
		),
		'Luck'     => array(
			'min' => 25,
			'max' => 40
		)
	);
	
	public static $orderusHealth;
	public static $orderusStrength;
	public static $orderusDefence;
	public static $orderusSpeed;
	public static $orderusLuck;
	
	public static $wildBeastHealth;
	public static $wildBeastStrength;
	public static $wildBeastDefence;
	public static $wildBeastSpeed;
	public static $wildBeastLuck;
	
	function __construct() {
		$this->genarateOrderusProperties();
		$this->genarateWildBeastProperties();
	}
	
	protected function genarateOrderusProperties() {
		$this->genarateProperties('orderus');
	}
	
	protected function genarateWildBeastProperties() {
		$this->genarateProperties('wildBeast');
	}
	
	protected function genarateProperties($propertyName = null) {
		if (isset(self::$$propertyName)) {
			foreach (self::$$propertyName as $key => $property) {
				$constName = $propertyName . $key;
				self::$$constName = $this->randomValue($property['min'], $property['max']);
			}
		}
	}
	
	protected function randomValue($min = null, $max = null) {
		if (!$min) {
			$min = self::MIN;
		}
		if (!$max) {
			$max = self::MAX;
		}
		return mt_rand($min, $max);
	}
	
	protected function calculateRand( $currentValue ) {
		$randValue = mt_rand(self::MIN, self::MAX);
		if ($randValue <= $currentValue) {
			return true;
		}
		return false;
	}
	
}